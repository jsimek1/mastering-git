#!/usr/bin/python3
# Copyright Contributors to the Packit project.
# SPDX-License-Identifier: MIT
"""
Homework check script.

Is it a good idea to make it open?
"""
import argparse
import os
import sys
import ogr
import re
from ogr.abstract import PRStatus
from ogr.exceptions import GitlabAPIException
from ogr.services.gitlab import GitlabProject

six_digit_number = re.compile(r'\d{6}')  # first 6 digits are matched
try:
    gitlab_service = ogr.GitlabService(token=os.environ["GITLAB_TOKEN"])
except IndexError:
    print("You need to set GITLAB_TOKEN env var to use this script.\n"
          "You can create one here: https://gitlab.com/-/profile/personal_access_tokens\n"
          "Read permissions are sufficient.")
    sys.exit(127)
# {'123456': ogr.GitLabProject}
# these folks did not put their forks into <username>/mastering-git, hence this mapping
# unfortunately gitlab doesn't allow (requires admin perms on the projects) to find this out via API
# more details in find_fork()
uco_fork_mapping = {
    "536533": GitlabProject(namespace="test3031739", repo="mastering-git", service=gitlab_service),
    "536494": GitlabProject(namespace="mastering-git", repo="pv-177-main", service=gitlab_service),
    "536589": GitlabProject(namespace="trilop753", repo="mastering-git-pv-177", service=gitlab_service),
    "514449": GitlabProject(namespace="AweSamDude", repo="mastering-git-my-fork", service=gitlab_service),
}


class Homework:
    pass


class Item:
    def __init__(self, gitlab_service: ogr.GitlabService,  username: str, uco: str):
        self.gitlab_service = gitlab_service
        self.username = username
        self.project = gitlab_service.get_project(namespace="redhat/research", repo="mastering-git")
        self.uco = uco
        self.fork = self.find_fork()

    def find_fork(self) -> GitlabProject:
        # this is the easiest: username/mastering-git
        probably_fork = self.gitlab_service.get_project(namespace=self.username, repo="mastering-git")
        if probably_fork.exists():
            return probably_fork
        # if the fork is in a different namespace than users, gitlab doesn't share owner ID of that
        # group or namespace, which is nuts! even when we have access, we cannot connect the username
        # and the particular fork; so we keep static list for these folks
        # this code would have done it:
        # # enumerate all forks of our repo and map them to users
        # # some forked to arbitrary namespaces
        # for fork in self.project.gitlab_repo.forks.list(get_all=True):
        #     fork = self.gitlab_service.gitlab_instance.projects.get(fork.id)
        #     username_fork_mapping[fork.owner['username']] = GitlabProject(repo=fork.path,
        #                                                                   namespace=fork.namespace['path'],
        #                                                                   service=self.gitlab_service)
        return uco_fork_mapping[self.uco]


class HasForkedMainRepo(Item):
    def check(self):
        try:
            # this doesn't do any calls yet
            fork = self.gitlab_service.get_project(repo="mastering-git", namespace=self.username)
            # this does
            fork.get_branches()
        except GitlabAPIException:
            return False
        else:
            return True


class HasPubkey(Item):
    def check(self):
        user = self.gitlab_service.gitlab_instance.users.list(username=self.username)[0]
        try:
            keys = user.keys.list()
            print(keys)
        except GitlabAPIException:
            return False
        else:
            return bool(keys)


class MRExists(Item):
    def check(self):
        prs = self.project.get_pr_list(PRStatus.all)
        for pr in prs:
            if pr.author == self.username:
                if pr.source_branch == 'main':
                    # we could halt here but there may be more MRs
                    # anyway, the request is to send the MR NOT from main
                    continue
                return True
        return False


class HasCommitThatChangesClass2HW(Item):
    def check(self):
        try:
            branches = self.fork.get_branches()
        except GitlabAPIException:
            return False
        for branch in branches:
            path = f"class2_homework/{self.uco}.txt"
            path_commands = f"class2_homework/{self.uco}_commands.txt"
            try:
                homework_content = self.fork.get_file_content(path, ref=branch)
                branch_commits = self.fork.gitlab_repo.commits.list(ref_name=branch, iterator=True)
                # this way we get *all* attributes, python-gitlab is weird
                top_commit = self.fork.gitlab_repo.commits.get(next(branch_commits).id)
                homework_commands = self.fork.get_file_content(path_commands, ref=branch)
                print(f"# {self.uco}")
                print(f"Author: {top_commit.author_name} <{top_commit.author_email}>")
                print(f"Message: {top_commit.message}")
                print(f"Date: {top_commit.created_at}")
                # some put really long stuff there, hence :128
                suffix = '...\n' if len(homework_content) >= 128 else '\n'
                print(f"```\n{homework_content[:128]}{suffix}```")
                print(f"## Commands\n{homework_commands}\n")
                input("Press Enter to continue: ")
                print("====================================")
            except FileNotFoundError:
                continue
            # both exist, we good
            return True
        return False


class Class2Homework(Homework):
    items = (HasCommitThatChangesClass2HW, )


def check_one(gitlab_username: str, uco: str):
    for HWKls in (Class2Homework,):
        for ItKls in HWKls.items:
            item = ItKls(gitlab_service, gitlab_username, uco)
            item.check()


def check_all(group: int):
    project = gitlab_service.get_project(namespace="redhat/research", repo="mastering-git")
    # group 1 -> issue #2
    # group 2 -> issue #3
    issue_id = group + 1
    if issue_id not in (2, 3):
        raise RuntimeError("Select either group 1 or 2.")
    # students comment here their UCOs so we can pair GL account with UCO
    issue_comments = project.get_issue(issue_id).get_comments()
    for issue_comment in issue_comments:
        # extract UCO with a fancy regex
        gitlab_username, uco = issue_comment.author, six_digit_number.findall(issue_comment.body)[0]
        check_one(gitlab_username, uco)


def check(group: int = None, gitlab_username: str = None, uco: str = None):
    """ entrypoint function; check one if specified, otherwise check all """
    if group:
        check_all(group=group)
    elif (gitlab_username and uco):
        check_one(gitlab_username, uco)
    else:
        raise RuntimeError("You need to set either --group or (--uco and --gitlab-username).")


def cli():
    """ CLI interface using argparse of this script """
    parser = argparse.ArgumentParser(description="Check 'Mastering Git' course homeworks.",
                                     epilog="You can check all homeworks of one group (1 or 2) or "
                                            "homework of one student by specifying username and UCO.")
    parser.add_argument("--group", type=int, required=False, choices=[1, 2],
                        help="Check homeworks of group with ID 1 or 2")
    parser.add_argument("--uco", type=str, required=False,
                        help="UCO identifier of the student (check only this homework)")
    parser.add_argument("--gitlab-username", type=str, required=False,
                        help="GitLab username of the student (check only this homework")

    args = parser.parse_args()

    return check(group=args.group, gitlab_username=args.gitlab_username, uco=args.uco)


if __name__ == '__main__':
    try:
        sys.exit(cli())
    except KeyboardInterrupt:
        print("\nBye\n")
