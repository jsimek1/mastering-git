I really like this course, mostly because it teaches something, which is utterly
needed in practise and common world, in contrast to some mandatory courses which
teach things I will never see again after university. The fact that it lasts only
half semester is also great, because I will have more time during the end of semester
where exams are dangerously close. And last thing, this course takes acceptable
amount of time for what it gives.